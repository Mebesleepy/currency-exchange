var app = new Vue({
  el: "#exchange",
  data() {
    return {
      data: null,
      exg: "SEK",
      selectedFromCurrency: {
        name: "",
        value: 0,
      },
      selectedToCurrency: {
        name: "",
        value: 0,
      },
      inputAmount: 0,
      outputAmount: 0,
    };
  },
  created() {
    this.getExchangeData();
  },
  mounted() {},
  updated() {},
  methods: {
    async getExchangeData() {
      await axios("https://api.exchangeratesapi.io/latest")
        .then((res) => {
          let arrayOfRates = [];

          for (const key in res.data.rates) {
            if (res.data.rates.hasOwnProperty(key)) {
              arrayOfRates.push({
                name: key,
                data: res.data.rates[key],
              });
            }
					}
					
          this.data = arrayOfRates;
        })
        .catch((error) => console.log(error));
    },
    setSelectedRate(rateName) {
      this.exg = rateName;
    },
    switchCurrency() {
      // Ye old switcheroo
      let temp = { ...this.selectedFromCurrency };
      this.selectedFromCurrency = { ...this.selectedToCurrency };
      this.selectedToCurrency = { ...temp };
    },

    // calculate and round the output to 2 decimals
    calculateExchange() {
      const amount = this.inputAmount;
      const from = this.selectedFromCurrency.value;
      const to = this.selectedToCurrency.value;

      if (amount > 0) {
        let longNumber = (to / from) * amount;
        this.outputAmount = longNumber.toFixed(2);
      } else {
        this.outputAmount = 0;
      }
    },
  },
});
